import React from 'react'
import DuckImage from '../assets/Duck.jpg'
import classes from './HomeView.scss'

import StocksForm from '../../../containers/StocksForm';

export const HomeView = () => (
  <div>
    <h4>Welcome!</h4>
    <StocksForm/>
  </div>
)

export default HomeView
