import React, { PropTypes, Component } from 'react';
import { reduxForm } from 'redux-form';

import classes from './StocksForm.scss';

export const fields = [
  'stocks[].sid', 
  'stocks[].sDate', 
  'stocks[].eDate'
];

class StocksForm extends Component {

  handleSubmit(data) {
    //do stuff here
    console.log(data);
  }
  
  render() {

    const {
      fields: { stocks },
      handleSubmit,
    } = this.props;

    return (
      <form className={classes.stockForm} onSubmit={handleSubmit(this.handleSubmit)}>
        <button className='btn btn-default' type='button' onClick={() => stocks.addField()}>
          Add
        </button>
        {stocks.map((stock, index) => (
        <div key={index}>

          <input type='text' placeholder='Stock ID' {...stock.sid}/>
          <input type='text' placeholder='Beginning Date' {...stock.sDate}/>
          <input type='text' placeholder='End Date' {...stock.eDate}/>
          <button className='btn btn-warning' onClick={() => stocks.removeField(index)}>
            Delete
          </button>
        </div>

        ))

        }
        <button className='btn btn-default' type='submit'>
          Submit
        </button>
      </form>
    );
  }
}

export default reduxForm({
  form: 'stocks',
  fields,
})(StocksForm);

